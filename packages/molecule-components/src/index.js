import Heading from './src/components/Heading';
import Text from './src/components/Text';

export {
  Heading,
  Text
}