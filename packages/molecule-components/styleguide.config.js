const webpackConfig = require('../../webpack.config');

module.exports = {
	title: 'Molecule components',
  components: 'src/components/**/*.js',
  webpackConfig,
};