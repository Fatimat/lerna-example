import React from 'react';
import {Heading} from '@lerna-example/molecule-components';

const Title = () => (
  <>
  <h1>Title</h1>
  <Heading/>
  </>
)

export default Title;