import Title from './src/components/Title/Title';
import Paragraph from './src/components/Paragraph/Paragraph';

export {
  Title,
  Paragraph
}