const webpackConfig = require('../../webpack.config');

module.exports = {
	title: 'React Style Guide Example',
  components: 'src/components/**/*.js',
  webpackConfig,
};