const path= require('path');
const babelConfig = require('./babel.config');

module.exports = {
  entry: './src/index.js',
	output: {
    publicPath: '/build',
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'build'),
	},
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: babelConfig
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true
            }
          },
        ],
      },
    ],
  },
}