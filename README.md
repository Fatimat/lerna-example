# Lerna investigation

## What is lerna

Lerna is a library that provides ability to manage multi-repository structure inside a single repository. A repository structured in this way is called a mono-repo. With lerna, we can have a single repo and ability to publish the different "packages" in that repo as seperate NPM package. 

For example, in this repo we have two packages: atom-components and molecule-components. They are both published on npm as two seperate packeages:[atom-components npm package](https://www.npmjs.com/package/@lerna-example/atom-components) and [molecule-components npm package](https://www.npmjs.com/package/@lerna-example/molecule-components)


## How it works

For a detailed explanation about lerna, I recommend watching [this](https://www.youtube.com/watch?v=p6qoJ4apCjA&t=539s) video. 


## Why it would be good for Apolitical Styleguide

Right now, styleguide is deployed as one massive package. However, within the repo, we do group our components into 3 different groups. It would make sense to still have the one repo, but publish the components to NPM as 3/4 seperate packages. This way, the size would reduce dramatically and we will have seperation of concerns with the different groups of component. 


## Some notes about adopting in styleguide

- It is not advisable to have circular dependencies within packages. Lerna does not know how to resolve this. See more [this](https://github.com/lerna/lerna/issues/306).So this means we might not need to have like a shared undeplpoyed folder for stuff. Keeping components independent will be great. This might be a problem for adopting in styleguide. 

- Some configurations changes would be needed in our gitlab ci

## Running locally

- Run `yarn install` in root repo
- Change directory to the package you want to run 
- Run `yarn styleguidist server` 