// @flow
module.exports = {
  presets: [
    ['@babel/env', { targets: ['last 2 versions', 'ie 11'] }],
    '@babel/react',
  ],
  plugins: [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-syntax-dynamic-import',
  ],
};
